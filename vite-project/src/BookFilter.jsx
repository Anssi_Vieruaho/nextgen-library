import React, { useState } from 'react';
import "./BookFilter.css"

const BookFilter = ({ books, updateFilteredBooks }) => {
    const [selectedGenre, setSelectedGenre] = useState('all');
    //handles the genre change when user selects genre from dropdown
    const handleGenreChange = (event) => {
        setSelectedGenre(event.target.value);
        const filtered = event.target.value === 'all' ? books : books.filter(book => book.genre === event.target.value);
        updateFilteredBooks(filtered);
    };

    return (
        <div className="Filter">
            <select className="selector" value={selectedGenre} onChange={handleGenreChange}>
                <option value="all">All Genres</option>
                <option value="Children">Children</option>
                <option value="Fantasy">Fantasy</option>
                <option value="History">History</option>
                <option value="Horror">Horror</option>
                <option value="Mystery">Mystery</option>
                <option value="Philosophy">Philosophy</option>
                <option value="Romance">Romance</option>
                <option value="Sci-fi">Sci-fi</option>
                <option value="Self-help">Self-help</option>
                <option value="Western">Western</option>
            </select>
        </div>
    );
};

export default BookFilter;