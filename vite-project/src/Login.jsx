import { useEffect, useState } from "react";
import axios from "axios";
import "./Login.css"

export default function Login({ token, setToken, username, setUsername, password, setPassword, isLoggedIn, setIsLoggedIn, onLogin }) {
  const [error, setError] = useState("");

  useEffect(() => {
  }, [isLoggedIn]);

  //Login-function uses API:s post method and adds new user.
  const login = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/users/login';
    try {
      const response = await axios.post(url, { username, password });
      setToken(response.data.token);
      setIsLoggedIn(true);
      setPassword("");
      setUsername("");
      setError("");
      onLogin(response.data.token);
    } catch (error) {
      setError("Incorrect username or password");
      setPassword("");
      setUsername("");
    }
  };

  //If user is not logged in, these items will be shown.
  if (isLoggedIn === false) {
    return (
      <div>
        <h2 className="loginh2">Log in</h2>
        <label>
          Username:
          <input type='text' value={username} onChange={event => setUsername(event.target.value)} />
        </label>
        <br />
        <label>
          Password:
          <input type='password' value={password} onChange={event => setPassword(event.target.value)} />
        </label>
        <br />
        {error && <p>{error}</p>}
        <div className="Login-gridi">
          <button className="LoginButton" onClick={login}>Log in</button>
        </div>
      </div>
    );
  }
}