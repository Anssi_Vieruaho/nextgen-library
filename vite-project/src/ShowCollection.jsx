import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './ShowCollection.css';
import Fantasy from "./assets/fantasy.jpg";
import Mystery from "./assets/mystery.jpg";
import Romance from "./assets/romance.jpg";
import Children from "./assets/children.jpg";
import History from "./assets/history.jpg";
import Horror from "./assets/horror.jpg";
import Philosophy from "./assets/philosophy.jpg";
import Scifi from "./assets/scifi.jpg";
import Selfhelp from "./assets/selfhelp.jpg";
import Western from "./assets/western.jpg";
import BookFilter from './BookFilter';
import SingleBook from "./assets/singlebook.jpg"

const genreImages = {
  fantasy: Fantasy,
  mystery: Mystery,
  romance: Romance,
  children: Children,
  history: History,
  horror: Horror,
  philosophy: Philosophy,
  scifi: Scifi,
  selfhelp: Selfhelp,
  western: Western,
  singlebook: SingleBook
};

// removes - in genre names self-help and sci-fi so that comparing works
const standardizeGenreName = (genre) => {
  return genre.toLowerCase().replace(/[^a-z0-9]/g, '');
};

function ShowCollection({ isLoggedIn, token }) {
  const [books, setBooks] = useState([]);
  const [filteredBooks, setFilteredBooks] = useState([]);
  const [error, setError] = useState(null);
  const [loan, setLoan] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  const booksPerPage = 24;

  useEffect(() => {
    const fetchBooks = async () => {
      try {
        const response = await axios.get('https://buutti-library-api.azurewebsites.net/api/books');
        const sortedBooks = sortAuthorsAlphabetically(response.data);
        setBooks(sortedBooks);
        setFilteredBooks(sortedBooks);
      } catch (error) {
        setError(error);
      }
    };

    fetchBooks(); {/* this function will bring the booklist from server */ }
  }, []);

  const sortAuthorsAlphabetically = (books) => {
    return books.sort((a, b) => {
      if (!a.author && !b.author) {
        return 0;
      }
      if (!a.author) {
        return 1;
      }
      if (!b.author) {
        return -1;
      }
      return a.author.localeCompare(b.author);
    });
  }; {/* this function will sort the publications alphabetically based on authors first name  */ }

  // loans a book from collection
  const loanBook = async (isbn) => {
    try {
      const url = `https://buutti-library-api.azurewebsites.net/api/loans/${isbn}`;
      const config = {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
      const response = await axios.post(url, {}, config);
      setLoan([...loan, response.data]);

      alert("Book loaned successfully!");
      console.log(response.data);
      location.reload() //refresh the page because otherwise book doesn't appear to loaned books(front end)
    } catch (error) {
      console.error("Error loaning book:", error);
      alert("Failed to loan the book. Please try again.");
    }
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const getPaginatedBooks = () => {
    const startIndex = (currentPage - 1) * booksPerPage;
    const endIndex = startIndex + booksPerPage;
    return filteredBooks.slice(startIndex, endIndex);
  };

  const totalPages = Math.ceil(filteredBooks.length / booksPerPage);
  const paginatedBooks = getPaginatedBooks();

  // update filtered books
  const updateFilteredBooks = (filteredBooks) => {
    setFilteredBooks(filteredBooks);
    setCurrentPage(1);
  };

  return (
    <div className="ShowCollection"> {/* publications from API will be shown in here */}
      <h1>Collection</h1>
      <BookFilter books={books} updateFilteredBooks={updateFilteredBooks} />
      {token ? (
        <p className="plainText">Take a look at our book collection. </p>
      ) : (
        <p className='plainText'>Take a look at our book collection. Please log in to loan a book. </p>)}
      <div className="Gridcollection">
        {filteredBooks.length > 0 ? (
          <ul className="CollectionList">
            {paginatedBooks.map((book, index) => (
              <li key={index} className="CollectionListItem">
                <img src={genreImages[standardizeGenreName(book.genre)] || SingleBook} alt={book.genre} />
                {book.author ? `${book.author}, ` : ''}<br />{book.title},<br /> {book.year}, <br />{book.genre} {/*Tells the author's name, book's title, year and genre */}
                {isLoggedIn === true && token ? (
                  <button className='button-round1' onClick={() => loanBook(book.isbn)}>Loan</button>
                ) : (
                  <p></p> // not logged in so nothing is shown
                )}
              </li>
            ))}
          </ul>
        ) : (
          <p className="plainText">No books available</p>
        )}
      </div>
      <div className="pagination">
        {Array.from({ length: totalPages }, (_, index) => (
          <span id="paginate"
            key={index}
            onClick={() => handlePageChange(index + 1)}
            style={{
              cursor: 'pointer',
              fontSize: currentPage == index + 1 ? 18 : 16,
              fontWeight: currentPage === index + 1 ? 'bold' : 'normal',
            }}
            className={`pagination-button ${index + 1 === currentPage ? 'active' : ''}`}
          >
            {index + 1}
          </span>
        ))}
      </div>
    </div>
  );
}

export default ShowCollection;