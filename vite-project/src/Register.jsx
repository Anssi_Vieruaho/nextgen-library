import { useState } from "react";
import axios from "axios";
import "./Login.css"

export default function Register({ token, setToken, usernameR, setUsernameR, passwordR, setPasswordR, onRegister }) {
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [error, setError] = useState("");

  //Register function uses API:s post method and registers new user, if username hasn't already been taken.
  const register = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/users/register';
    const username = usernameR;
    const password = passwordR;
    try {
      const response = await axios.post(url, { username, password });
      setToken(response.data.token);
      setUsernameR("");
      setPasswordR("");
      setRegistrationSuccess(true); // Set registration success to true
      setError("");
    } catch (error) {
      setError("Username or password has already been taken")
      setUsernameR("");
      setPasswordR("");
    }
  };

  //These items will be shown, if registrationSuccess is false.
  return (
    <div>
      <h2 className="loginh2">Register</h2>
      {!registrationSuccess ? (
        <>
          <label>
            Username:
            <input type='text' value={usernameR} onChange={event => setUsernameR(event.target.value)} />
          </label>
          <br />
          <label>
            Password:
            <input type='password' value={passwordR} onChange={event => setPasswordR(event.target.value)} />
          </label>
          <br />
          {error && <p>{error}</p>}
          <button className="LoginButton" onClick={register}>Register</button>
        </>
      ) : (
        <>
          <p className="plainTextb">Registration successful!</p>
          <button className="LoginButton" onClick={onRegister}>Close</button>
        </>
      )}
    </div>
  );
}