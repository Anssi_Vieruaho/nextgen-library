import { useState, useEffect } from 'react';
import axios from 'axios';
import './OwnLoans.css';
import Fantasy from "./assets/fantasy.jpg";
import Mystery from "./assets/mystery.jpg";
import Romance from "./assets/romance.jpg";
import Children from "./assets/children.jpg";
import History from "./assets/history.jpg";
import Horror from "./assets/horror.jpg";
import Philosophy from "./assets/philosophy.jpg";
import Scifi from "./assets/scifi.jpg";
import Selfhelp from "./assets/selfhelp.jpg";
import Western from "./assets/western.jpg";
import SingleBook from "./assets/singlebook.jpg";

const genreImages = {
  fantasy: Fantasy,
  mystery: Mystery,
  romance: Romance,
  children: Children,
  history: History,
  horror: Horror,
  philosophy: Philosophy,
  scifi: Scifi,
  selfhelp: Selfhelp,
  western: Western,
  singlebook: SingleBook
};

const genreImages2 = {
  Fantasy: Fantasy,
  Mystery: Mystery,
  Romance: Romance,
  Children: Children,
  History: History,
  Horror: Horror,
  Philosophy: Philosophy,
  Scifi: Scifi,
  Selfhelp: Selfhelp,
  Western: Western,
  Singlebook: SingleBook
};

// Standardize genre names to lowercase and remove non-alphanumeric characters
const standardizeGenreName = (genre) => {
  return genre.toLowerCase().replace(/[^a-z0-9]/g, '');
};

function OwnLoans(props) {
  const [loans, setLoans] = useState([{}]);
  const [view, setView] = useState('loaned');
  const [reviewText, setReviewText] = useState('');
  const [visibleTextareaIsbn, setVisibleTextareaIsbn] = useState(null);
  const [booksresult, setBooksresult] = useState([{}]);
  const [ownLoansList, setOwnLoansList] = useState([{}]);

  // Fetch own loans and books data when the component is installed
  useEffect(() => {
    getOwnLoans();
    fetchBooksOwn();
  }, []);

  // Update own loans list whenever loans or booksresult changes
  useEffect(() => {
    ownLoansListFunction();
  }, [loans, booksresult]);

  //This function uses API's get method to get all user's book loans and saves data to loan variable.
  const getOwnLoans = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/loans/';
    const config = {
      headers: {
        Authorization: 'Bearer ' + props.token,
      },
    };
    const response = await axios.get(url, config);
    setLoans(response.data);
  };

  //This function uses API:s get method and saves data to booksresult.
  const fetchBooksOwn = async () => {
    const result = await axios.get('https://buutti-library-api.azurewebsites.net/api/books');
    setBooksresult(result.data);
  };

  //This function filters and compare booksresult and loans based on their different key properties, but same values and check if book is returned.
  function ownLoansListFunction() {
    const ownlist = booksresult.filter(({ isbn }) =>
      loans.some(({ book_isbn, returned }) => isbn === book_isbn && !returned)
    );
    setOwnLoansList(ownlist);
  }

  return (
    <div className="OwnLoans">
      <h3 className="ownloanstitle">My Loans</h3>
      <br />
      <button className='Loaned' onClick={() => setView('loaned')}>Loaned Books</button>
      <button className='Returned' onClick={() => setView('returned')}>Returned Books</button>
      {view === 'loaned' ? (
        <LoanedBooks
          loans={loans}
          setLoans={setLoans}
          ownLoansList={ownLoansList}
          token={props.token}
          visibleTextareaIsbn={visibleTextareaIsbn}
          setVisibleTextareaIsbn={setVisibleTextareaIsbn}
          setReviewText={setReviewText}
          reviewText={reviewText}
        />
      ) : (
        <ReturnedBooks loans={loans} booksresult={booksresult} />
      )}
    </div>
  );
}

// Function to return a book
function LoanedBooks({ loans, token, visibleTextareaIsbn, setVisibleTextareaIsbn, reviewText, setReviewText, ownLoansList, setLoans }) {
  const returnBook = async (isbn) => {
    try {
      const url = `https://buutti-library-api.azurewebsites.net/api/loans/${isbn}`;
      const config = {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      };
      const response = await axios.put(url, {}, config);
      setLoans(loans.map((loan) => loan.book_isbn === isbn ? { ...loan, returned: true } : loan));
      alert('Book returned successfully!');
    } catch (error) {
      console.error('Error returning book:', error);
      alert('Failed to return the book. Please try again.');
    }
  };

  // Function to handle review text change
  const handleReviewChange = (event) => {
    setReviewText(event.target.value);
  };

  // Toggle visibility of the review text area
  const handleButtonClick = (isbn) => {
    if (visibleTextareaIsbn === isbn) {
      setVisibleTextareaIsbn(null);
    } else {
      setVisibleTextareaIsbn(isbn);
    }
  };

  // Function to submit a new review
  const newReview = async (isbn) => {
    const url = `https://buutti-library-api.azurewebsites.net/api/reviews/${isbn}`;
    const config = {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    };
    const data = {
      review: reviewText,
    };
    const response = await axios.post(url, data, config);
    setReviewText('');
    setVisibleTextareaIsbn(null);
  };

  return (
    <div className="LoanedBooks">
      <h3 className='ownloanstitle'>Loaned Books</h3>
      <p className='plaintext'>Your loaned books: {ownLoansList.length}</p>
      <ul>
        {/* Map over ownLoansList and display each loaned book */}
        {ownLoansList.length > 0 ? (
          ownLoansList.map((loan, index) => (
            <li key={index} className='loanedList'>
              <img src={genreImages2[(loan.genre)] || SingleBook} alt={loan.genre} />
              {console.log("genre", loan.genre)}
              {loan.author},<br /> {loan.title},<br /> {loan.year},<br /> {loan.genre}
              <br />
              <button className='loanstate' onClick={() => returnBook(loan.isbn)}>Return</button>
              <button className='loanstate' onClick={() => handleButtonClick(loan.isbn)}>
                {visibleTextareaIsbn === loan.isbn ? 'Close' : 'Write Review'}
              </button>
              {visibleTextareaIsbn === loan.isbn && (
                <div id="textAreaContainer">
                  <textarea
                    id="textArea"
                    placeholder="Write your review"
                    value={reviewText}
                    onChange={handleReviewChange}
                    autoFocus
                  ></textarea>
                  <br />
                  <button className="submitButton" onClick={() => newReview(loan.isbn)}>Submit</button>
                </div>
              )}
            </li>
          ))
        ) : (
          <h2 className='plaintext'>No loaned books</h2>
        )}
      </ul>
      <br />
    </div>
  );
}

// Filter books to find the ones that are returned
function ReturnedBooks({ loans, booksresult }) {
  const returnedBooksList = booksresult.filter(({ isbn }) =>
    loans.some(({ book_isbn, returned }) => isbn === book_isbn && returned)
  );

  return (
    <div>
      <h3 className='ownloanstitle'>Returned Books</h3>
      <p className='plaintext'>Your returned books: {returnedBooksList.length}</p>
      <ul>
        {/* Map over returnedBooksList and display each returned book */}
        {returnedBooksList.length > 0 ? (
          returnedBooksList.map((book, index) => (
            <li key={index} className='loanedList' >
              <img src={genreImages[standardizeGenreName(book.genre)] || SingleBook} alt={book.genre} />
              {book.author},<br /> {book.title},<br /> {book.year},<br /> {book.genre}
              <br />
            </li>
          ))
        ) : (
          <h2>No returned books</h2>
        )}
      </ul>
    </div>
  );
}

export default OwnLoans