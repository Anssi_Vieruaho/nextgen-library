import { useState, useEffect } from 'react';
import axios from 'axios';
import './Carousel.css';
import WidePileOfBooks from "./assets/widepileofbooks.jpg"

export default function Carousel(props) {
    const [index, setIndex] = useState(0);
    const [reviews, setReviews] = useState([{}]);
    const [error, setError] = useState(null)
    const [booksresult, setBooksresult] = useState([{}])
    const [reviewTitle, setReviewTitle] = useState([{}])

    useEffect(() => {
        const getReviews = async () => {
            try {
                const url = 'https://buutti-library-api.azurewebsites.net/api/reviews';
                const response = await axios.get(url);
                setReviews([...response.data]);
            } catch (error) {
                setError(error)
            }
        };
        getReviews();
    }, []);

    useEffect(() => {
        const intervalId = setInterval(() => {
            setIndex((prevIndex) => (prevIndex + 1) % reviews.length);
        }, 4000);
        return () => clearInterval(intervalId);
    }, [reviews.length]);

    useEffect(() => {
        fetchAllBooks()
    }, []);

    useEffect(() => {
        if (reviews.length > 0 && booksresult.length > 0) {
            compareLists();
        }
    }, [reviews, booksresult]);

    const rotate = (n) => () => setIndex((index + reviews.length + n) % reviews.length);

    //This function fetch all books and saves data to booksresult.
    const fetchAllBooks = async () => {
        const result = await axios.get('https://buutti-library-api.azurewebsites.net/api/books')
        setBooksresult(result.data)
    }

    //This function filters and compares booksresult and saves data to reviewTitle.
    function compareLists() {
        const reviewTitles = booksresult.filter(({ isbn }) =>
            reviews.some(({ book_isbn }) => isbn === book_isbn)
        )
        setReviewTitle(reviewTitles)
    }

    return (
        <div className="Carousel">
            <span><h2>What readers have said about our books</h2></span>
            <img src={WidePileOfBooks} className="CarouselBooks" />
            {reviews.length > 0 && (
                <div className="review">
                    <p className='reviewTitle'>{reviewTitle[index]?.title}</p>
                    <p className="review">"{reviews[index].review}"</p>
                    <p className="user">{reviews[index].username}</p>
                </div>
            )}
        </div>
    );
}