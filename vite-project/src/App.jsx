import { useState, useEffect } from 'react';
import './App.css';
import ShowCollection from "./ShowCollection.jsx";
import Login from "./Login.jsx";
import Register from "./Register.jsx";
import Footer from "./Footer.jsx";
import Header from "./Header.jsx";
import Carousel from "./Carousel.jsx";
import { FaUser, FaUserPlus, FaUserTimes } from "react-icons/fa"
import OwnLoans from "./OwnLoans.jsx";
import Modal from 'react-modal';

Modal.setAppElement('#root');

function App() {
  const [active, setActive] = useState("");
  const [token, setToken] = useState(localStorage.getItem('token')); // Retrieves the token from local storage if it exists
  const [usernameR, setUsernameR] = useState('');
  const [passwordR, setPasswordR] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(localStorage.getItem('isLoggedIn') === 'true'); // Retrieves and converts the login status from local storage to a boolean
  const [visible, setVisible] = useState(false);
  const [reviews, setReviews] = useState([{}]);
  const [error, setError] = useState("");

  useEffect(() => {
    if (token !== null) {
      setVisible(true);
      localStorage.setItem('token', token); // Store the token in local storage
    } else {
      localStorage.removeItem('token'); // Remove the token from local storage
    }
  }, [token, visible]);

  useEffect(() => {
    const getReviews = async () => {
      try {
        const url = 'https://buutti-library-api.azurewebsites.net/api/reviews';
        const response = await axios.get(url);
        setReviews([...response.data]);
      } catch (error) {
        setError(error);
      }
    };
    getReviews();
  }, []);

  const handleLogout = () => {
    setToken(null);
    setIsLoggedIn(false);
  };

  const handleLogin = (token) => {
    setToken(token);
    setIsLoggedIn(true);
    setActive("");
  };

  const handleRegister = () => {
    setActive("");
  };

  return (
    <div>
      <div className="App1">
        <div className="Grid-container">
          <div className="Header1"><Header />
            {isLoggedIn === false &&
              <h3><FaUserPlus className="RegButton" onClick={() => { console.log("Register clicked"); setActive("RegisterPage"); }} /></h3>}
            {isLoggedIn === false && <h3><FaUser className="LogoutButton" onClick={() => { console.log("Login clicked"); setActive("LoginPage"); }} /></h3>}
            {isLoggedIn && <h3><FaUserTimes onClick={handleLogout} className="LogoutButton" /></h3>}</div>

          <div className="Slider"><Carousel reviews={reviews} setReviews={setReviews} /></div>
          <div className="Books-grid">
            <ShowCollection token={token} setToken={setToken} isLoggedIn={isLoggedIn} setIsLoggedIn={setIsLoggedIn} />
          </div>
          <div className='Loans-grid'>
            {isLoggedIn === true && <OwnLoans token={token} setToken={setToken} reviews={reviews} setReviews={setReviews} />}
          </div>
          <div className="FooterGrid"><Footer /></div>
        </div>
      </div>

      <Modal
        isOpen={active === "LoginPage"}
        onRequestClose={() => setActive("")}
        contentLabel="Login Modal"
        className="Modal"
        overlayClassName="Overlay"
      >
        <Login
          title="LoginPage"
          token={token}
          setToken={setToken}
          username={username}
          setUsername={setUsername}
          password={password}
          setPassword={setPassword}
          isLoggedIn={isLoggedIn}
          setIsLoggedIn={setIsLoggedIn}
          onLogin={handleLogin}
        />
      </Modal>

      <Modal
        isOpen={active === "RegisterPage"}
        onRequestClose={() => setActive("")}
        contentLabel="Register Modal"
        className="Modal"
        overlayClassName="Overlay"
      >
        <Register
          title="RegisterPage"
          token={token}
          setToken={setToken}
          usernameR={usernameR}
          setUsernameR={setUsernameR}
          passwordR={passwordR}
          setPasswordR={setPasswordR}
          onRegister={handleRegister} // Pass the handleRegister function
        />
      </Modal>
    </div>
  );
}

export default App;
