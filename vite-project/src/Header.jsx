import "./Header.css"

export default function Header() {
    return (
        <div className="Header">
            <h1>NextGen Library</h1>
        </div>
    )
}